# Word PIN

## Objectives

The purpose of this repository is to let you make a list of PINs that
will be difficult for anyone else to guess or learn without your
consent.

The worst case for you is if someone can watch you enter your PIN (but
not see the exact arrangement of the pin pad).

- They might have seen the positions of all but 2 digits on the matrix
  you're using to enter your PIN, or they might have seen the positions
  of fewer numbers.
- They might be able to track the order you tap each digit and remember
  when you tap the same one again (which would mean your only defense is
  that they don't know what digit it actually is, just where the same
  digit re-appears in your PIN), or they might not be so proficient at
  that.

To defend against these situations, a good PIN will have certain
features:

- It should be difficult to learn the digit used at each position in the
  PIN
  - The PIN should use all the digits available (probably 0-9), possibly
    except for one (if someone knows the position of 9 out of the 10
    possible digits, they probably know the 10th digit is wherever the
    other digits aren't, so using a 10th digit doesn't make it much less
    likely an attacker can learn your PIN). This makes it so an attacker
    has to learn the position of every digit as you're entering your
    PIN.
  - The PIN should use at least 2 digits very rarely (but still use
    them), and use at least 2 digits very often. This makes it so an
    attacker is less likely to be able to learn what the two rare digits
    are or what order they're in, because they will have fewer chances
    to do so.
- It should be difficult to learn when digits are repeated within the
  PIN
  - The PIN should use a complicated order of digits that is hard to
    notice patterns in

Currently, the qualities of a PIN are measured by testing how easily it
is compressed: a PIN that uses many unique digits in a confusing order
will be harder to compress into a small file, as the producers of
compression software are incentivized to be able to summarize many
different types of patterns.

## Future Directions

- Using a "backspace" or "cancel" button as part of the sequence
  - This might be useful to make a longer PIN that is more likely to
    confuse an attacker. A PIN of `1234567899<backspace>0` or
    `1234567890<cancel>1029384756` might make it less likely an attacker
    will understand what you're tapping.
